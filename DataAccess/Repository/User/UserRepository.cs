using System;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using OnlineBanking.DataAccess.Entities;
using Microsoft.Extensions.Configuration;
using Dapper;

namespace OnlineBanking.DataAccess
{
    public class UserRepository : IUserRepository
    {
        #region Member Variables
        
        private readonly IConfiguration _configuration;
        
        public IDbConnection Connection
        {
            get  
            {
                return new SqlConnection(_configuration["ConnectionStrings:OnlineBanking"]);
            }
        }

        #endregion

        #region Constructors
        
        public UserRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        #endregion

        #region Operations

        public bool Add(User user)
        {
            var success = false;

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var transaction = dbConnection.BeginTransaction();
                
                try
                {
                    var sql = "INSERT INTO UserInfo (UserId, FirstName, LastName, Email, Password, PasswordSalt) " +
                              "VALUES(@UserId, @FirstName, @LastName, @Email, @Password, @PasswordSalt)";
                    
                    dbConnection.Execute(sql, user, transaction);

                    foreach(var item in user.SecurityQuestions)
                    {
                        sql = "INSERT INTO UserSecurityQuestion(UserId, QuestionId, Answer) " +
                              "VALUES (@UserId, @QuestionId, @Answer)";
                        
                        dbConnection.Execute(sql, new { UserId = user.UserId, QuestionId = item.Key, Answer = item.Value }, transaction );
                    }

                    transaction.Commit();
                    success = true;
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
            }

            return success;
        }
        
        public bool Update(User user)
        {
            var success = false;

            using (IDbConnection dbConnection = Connection)
            {
                var sql =   "UPDATE UserInfo " +
                            "SET FirstName = @FirstName, LastName = @LastName, Password = @Password, Email = @Email " +
                            "WHERE UserId = @UserId";
                
                dbConnection.Open();
                success = dbConnection.Execute(sql, user) > 0;
            }

            return success;
        }
        
        public bool Delete(Guid userId)
        {
            var success = false;

            using (IDbConnection dbConnection = Connection)
            {
                var sql =   "DELETE FROM UserInfo " +
                            "WHERE UserId = @UserId";
                
                dbConnection.Open();
                success = dbConnection.Execute(sql, new { UserId = userId }) > 0;
            }

            return success;
        }
        
        public User Get(Guid userId)
        {
            User user = null;
            
            using (IDbConnection dbConnection = Connection)
            {
                var sql =   "SELECT * " +
                            "FROM UserInfo " +
                            "WHERE UserId = @UserId";
                
                dbConnection.Open();
                user = dbConnection.Query<User>(sql, new { UserId = userId })
                    .FirstOrDefault();
            }

            return user;
        }

        public User GetByEmail(string email)
        {
            User user = null;
            
            using (IDbConnection dbConnection = Connection)
            {
                var sql =   "SELECT * " +
                            "FROM UserInfo " +
                            "WHERE Email = @Email";
                
                dbConnection.Open();
                user = dbConnection.Query<User>(sql, new { Email = email })
                    .FirstOrDefault();
            }

            return user;
        }
        
        public List<User> GetAll()
        {
            List<User> result = null;
            
            using (IDbConnection dbConnection = Connection)
            {
                var sql =   "SELECT UserId, FirstName, LastName, Email " +
                            "FROM UserInfo ";
                
                dbConnection.Open();
                result = dbConnection.Query<User>(sql).ToList();
            }

            return result;
        }

        #endregion
    }
}