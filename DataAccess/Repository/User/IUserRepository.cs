using System;
using System.Collections.Generic;
using OnlineBanking.DataAccess.Entities;

namespace OnlineBanking.DataAccess
{
    public interface IUserRepository
    {
        bool Add(User entity);
        bool Update(User entity);
        bool Delete(Guid userId);
        User Get(Guid userId);
        User GetByEmail(string email);
        List<User> GetAll();
    }
}