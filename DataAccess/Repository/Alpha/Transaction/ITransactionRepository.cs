using System;
using System.Collections.Generic;
using OnlineBanking.DataAccess.Entities;

namespace OnlineBanking.DataAccess
{
    public interface ITransactionRepository
    {
        Transaction[] GetAll(Guid accountId);
    }
}