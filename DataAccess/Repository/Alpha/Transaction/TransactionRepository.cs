using System;
using System.Linq;
using System.Collections.Generic;
using OnlineBanking.DataAccess.Entities;
using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace OnlineBanking.DataAccess
{
    public class TransactionRepository : ITransactionRepository
    {
        #region Member Variables
        
        private readonly IConfiguration _configuration;
        private Transaction[] _transactionList;

        #endregion

        #region Constructors
        
        public TransactionRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _transactionList = getTransactionList();
        }

        #endregion

        #region Operations
        
        public Transaction[] GetAll(Guid accountId)
        {
            return _transactionList
                .Where(t => t.AccountId == accountId)
                .ToArray();
        }

        #endregion

         #region Helper Methods

        private Transaction[] getTransactionList()
        {
            var path = @"C:\src\OnlineBanking\DataAccess\Repository\Alpha\Transaction\Transaction.json";
            var json = System.IO.File.ReadAllText(path);
            var result = JsonConvert.DeserializeObject<Transaction[]>(json);
            return result;
        }

        #endregion
    }
}