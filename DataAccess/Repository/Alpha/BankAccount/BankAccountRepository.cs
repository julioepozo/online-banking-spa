using System;
using System.Linq;
using System.Collections.Generic;
using OnlineBanking.DataAccess.Entities;
using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace OnlineBanking.DataAccess
{
    public class BankAccountRepository : IBankAccountRepository
    {
        #region Member Variables
        
        private readonly IConfiguration _configuration;
        private BankAccount[] _accountList;
        
        #endregion

        #region Constructors
        
        public BankAccountRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _accountList = getAccountList();
        }

        #endregion

        #region Operations
        
        public BankAccount Get(Guid id)
        {
            return _accountList.Where(a => a.Id == id).SingleOrDefault();
        }
        
        public BankAccount[] GetAll(Guid userId)
        {
            return _accountList.Where(u => u.UserId == userId).ToArray();
        }

        #endregion

        #region Helper Methods

        private BankAccount[] getAccountList()
        {
            var path = @"C:\src\OnlineBanking\DataAccess\Repository\Alpha\BankAccount\BankAccount.json";
            var json = System.IO.File.ReadAllText(path);
            var result = JsonConvert.DeserializeObject<BankAccount[]>(json);
            return result;
        }

        #endregion
    }
}