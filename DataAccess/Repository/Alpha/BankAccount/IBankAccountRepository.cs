using System;
using System.Collections.Generic;
using OnlineBanking.DataAccess.Entities;

namespace OnlineBanking.DataAccess
{
    public interface IBankAccountRepository
    {
        BankAccount Get(Guid Id);
        BankAccount[] GetAll(Guid userId);
    }
}