using System;

namespace OnlineBanking.DataAccess.Entities
{
    public class BankAccountType
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool IsDebit { get; set; }
    }
}