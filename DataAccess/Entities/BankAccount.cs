using System;

namespace OnlineBanking.DataAccess.Entities
{
    public class BankAccount
    {
        public Guid UserId { get; set; }
        public Guid Id { get; set; }
        public string Description { get; set; }
        public BankAccountType Type { get; set; }
        public string ReferenceNumber { get; set; }
        public float Balance {get; set; }
    }
}