using System;

namespace OnlineBanking.DataAccess.Entities
{
    public class Transaction
    {
        public Guid AccountId { get; set; }
        public Guid Id { get; set; }
        public string Description { get; set; }
        public TransactionType Type { get; set; }
        public TransactionSource Source { get; set; }
        public DateTime Date { get; set; }
        public float Amount {get; set; }
    }
}