using System;

namespace OnlineBanking.DataAccess.Entities
{
    public class TransactionSource
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
    }
}