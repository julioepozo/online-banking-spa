namespace OnlineBanking.DataAccess.Entities
{
    public class SecurityQuestion
    {
        public string Id { get; set; }
        public string Description { get; set; }
    }
}