
The purpose of this application is to interface with banking core software and implement online banking functionality.

  Layers:
  
  ![Scheme](Stack.jpg)

  Authentication:

  - JWT
  
  User Store
  
  - SQL Server Database

**Notes**

1. There is a database dependency for the user store. It should be installed on an SQL Server instance.
   The database script creation script is in folder \DataAccess\Db

2. This application does not implement a banking core. To integrate with a core banking software, the Data Access Layer should implement it by honoring the provided interfaces.
   The reference implementation is developed using hard-coded json files, which simulate data that would be returned by a banking core.
   
