using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace OnlineBanking.WebApi.Model
{
    public class User
    {
        [Required]
        [MinLength(2)]
        public string FirstName { get; set; }
        [Required]
        [MinLength(2)]
        public string LastName { get; set; }
        [Required]
        [RegularExpression(@"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,10}$")]
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        [Compare("Password")]
        public string PasswordConfirmation { get; set; }
        [Required]
        [RegularExpression(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")]
        public string Email { get; set; }
        public Dictionary<string, string> SecurityQuestions { get; set; }

        public bool IsLocked { get; }
        public bool IEnabled { get; }
    }
}