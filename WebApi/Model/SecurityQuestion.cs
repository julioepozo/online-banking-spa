using System.ComponentModel.DataAnnotations;

namespace OnlineBanking.WebApi.Model
{
    public class SecurityQuestion
    {
        public string Id { get; set; }
        public string Description { get; set; }
    }
}