using System.ComponentModel.DataAnnotations;

namespace OnlineBanking.WebApi.Model
{
    public class Login
    {
        [Required]
        public string Email { get; set; }
        
        [Required]
        public string Password { get; set; }
    }
}