﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Cors;

namespace Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            setupSecurity(services);           
            registerTypes(services);  
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors
            (
                options => options.AllowAnyOrigin() // TODO: restrict this more
                    .AllowAnyMethod()
                    .AllowAnyHeader()
            );

            app.UseAuthentication();
            app.UseMvc();
        }

        #region Helper Methods

        private void setupSecurity(IServiceCollection services)
        {
            services.AddAuthorization(options => 
            {
                options.AddPolicy("Bearer", new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser().Build());
            });

            services.AddAuthentication(options => 
            {
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options => 
            {
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidIssuer = this.Configuration["JwtSettings:Issuer"],
                    ValidAudience = this.Configuration["JwtSettings:Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this.Configuration["JwtSettings:PublicKey"])),
                    ValidateIssuerSigningKey = true
                };
            });
        }

        private void registerTypes(IServiceCollection services)
        {
            services.AddTransient<
                OnlineBanking.DataAccess.IUserRepository, 
                OnlineBanking.DataAccess.UserRepository>();
            services.AddTransient<
                OnlineBanking.Business.IUserManager,
                OnlineBanking.Business.UserManager>();

            services.AddTransient<
                OnlineBanking.DataAccess.IBankAccountRepository, 
                OnlineBanking.DataAccess.BankAccountRepository>();
            services.AddTransient<
                OnlineBanking.Business.IBankAccountManager,
                OnlineBanking.Business.BankAccountManager>();

            services.AddTransient<
                OnlineBanking.DataAccess.ITransactionRepository, 
                OnlineBanking.DataAccess.TransactionRepository>();
            services.AddTransient<
                OnlineBanking.Business.ITransactionManager,
                OnlineBanking.Business.TransactionManager>();
        }

        #endregion
    }
}
