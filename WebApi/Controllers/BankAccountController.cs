﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Logging;
using WebApiLayer = OnlineBanking.WebApi.Model;
using BusinessLayer = OnlineBanking.Business.Model;
using OnlineBanking.Business;
using Microsoft.Extensions.Configuration;

namespace Api.Controllers
{
    [Authorize(Policy = "Bearer")]
    [Route("api/[controller]")]
    public class BankAccountController : Controller
    {
        #region Member Variables

        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly IBankAccountManager _manager;
        
        #endregion

        #region Constructors

        public BankAccountController(IConfiguration configuration, ILogger<UserController> logger, IBankAccountManager manager)
        {
            _configuration = configuration;
            _logger = logger;
            _manager = manager;
        }

        #endregion

        #region Operations

        [HttpGet("list/{userId}")]
        public IActionResult GetAll(string userId)
        {
            try
            {
                var list = _manager.GetAll(Guid.Parse(userId));

                if(list != null && list.Count > 0)
                    return Ok(list);
                else
                    return NotFound();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "Unable to get bank account list.");
                return StatusCode(500);
            }
        }

        [HttpGet("{id}")]
        public IActionResult Get(string id)
        {
            try
            {
                var bankAccount = _manager.Get(Guid.Parse(id));

                if(bankAccount != null)
                    return Ok(bankAccount);
                else
                    return NotFound();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, $"Unable to get account '{id}'");
                return StatusCode(500);
            }
        }

        #endregion

        #region Helper Methods

        #endregion
    }
}