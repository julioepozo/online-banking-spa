using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Logging;
using WebApiLayer = OnlineBanking.WebApi.Model;
using BusinessLayer = OnlineBanking.Business.Model;
using OnlineBanking.Business;
using Microsoft.Extensions.Configuration;

namespace Api.Controllers
{
    [Authorize(Policy = "Bearer")]
    [Route("api/[controller]")]
    public class TransactionController : Controller
    {
        #region Member Variables

        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly ITransactionManager _manager;
        
        #endregion

        #region Constructors

        public TransactionController(IConfiguration configuration, ILogger<UserController> logger, ITransactionManager manager)
        {
            _configuration = configuration;
            _logger = logger;
            _manager = manager;
        }

        #endregion

        #region Operations

        [HttpGet("{accountId}")]
        public IActionResult Get(string accountId)
        {
            try
            {
                if(string.IsNullOrEmpty(accountId))
                    return BadRequest();

                var list = _manager.GetAll(Guid.Parse(accountId));

                if(list != null && list.Count > 0)
                    return Ok(list);
                else
                    return NotFound();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, $"Unable to get transactions for account id '{accountId}'");
                return StatusCode(500);
            }
        }

        #endregion
    }
}