﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Logging;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using WebApiLayer = OnlineBanking.WebApi.Model;
using BusinessLayer = OnlineBanking.Business.Model;
using OnlineBanking.Business;
using Microsoft.Extensions.Configuration;
using OnlineBanking.Common.Security;

namespace Api.Controllers
{
    [Authorize(Policy = "Bearer")]
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        #region Member Variables

        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly IUserManager _manager;

        #endregion

        #region Constructors

        public UserController(IConfiguration configuration, ILogger<UserController> logger, IUserManager manager)
        {
            _configuration = configuration;
            _logger = logger;
            _manager = manager;
        }

        #endregion

        #region Operations
        
        [AllowAnonymous]
        [HttpPost]
        public IActionResult Post([FromBody] WebApiLayer.User model)
        {
            model.SecurityQuestions = new Dictionary<string, string>();
            model.SecurityQuestions.Add("AE0AB0EA-8424-42C5-B04B-25C8DF59137D", EncryptionHelper.GenerateSaltedHash("aaa", "pkk"));
            model.SecurityQuestions.Add("D5AA7C91-0259-4644-A846-F321AC71B1F8", EncryptionHelper.GenerateSaltedHash("bbb", "pkk"));
            
            try
            {
                if(!ModelState.IsValid)
                    return BadRequest();

                var blUser = new BusinessLayer.User()
                {
                    UserId = Guid.NewGuid(),
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Email = model.Email,
                    Password = model.Password,
                    SecurityQuestions = model.SecurityQuestions
                };

                var success = _manager.Add(blUser);

                if(success)
                    return Ok();
                else
                    return BadRequest();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, $"Unable to create user '{model.Email}'.");
                return StatusCode(500);
            }
        }

        [HttpPut("{userId}")]
        public IActionResult Put(string userId, [FromBody] WebApiLayer.User model)
        {
            try
            {
                if(!ModelState.IsValid)
                    return BadRequest();

                var blUser = new BusinessLayer.User()
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Email = model.Email,
                    Password = model.Password,
                    PasswordSalt = model.PasswordSalt
                };

                var success = _manager.Update(Guid.Parse(userId), blUser);

                if(success)
                    return Ok();
                else
                    return BadRequest();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, $"Unable to update user '{userId}'.");
                return StatusCode(500);
            }
        }

        [HttpDelete("{userId}")]
        public IActionResult Delete(string userId)
        {
            try
            {
                var success = _manager.Delete(Guid.Parse(userId));

                if(success)
                    return Ok();
                else
                    return NotFound();
            }
            catch(Exception ex)
            {
                 _logger.LogError(ex, $"Unable to delete user '{userId}.");
                return StatusCode(500);
            }
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var list = _manager.GetAll();

                if(list != null && list.Count > 0)
                    return Ok(list);
                else
                    return NotFound();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "Unable to get user list.");
                return StatusCode(500);
            }
        }

        [HttpGet("{userId}")]
        public IActionResult Get(string userId)
        {
            try
            {
                var user = _manager.Get(Guid.Parse(userId));

                if(user != null)
                    return Ok(user);
                else
                    return NotFound();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, $"Unable to get user '{userId}'");
                return StatusCode(500);
            }
        }

        [Route("authenticate")]
        [AllowAnonymous]
        [HttpPost]
        public IActionResult Authenticate([FromBody] WebApiLayer.Login model)
        {
            IActionResult result;
            
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();
                
                var authResult = _manager.Authenticate(model.Email, model.Password);

                switch(authResult.Status)
                {
                    case BusinessLayer.AccountStatus.NotFound :
                    case BusinessLayer.AccountStatus.Disabled :
                    case BusinessLayer.AccountStatus.Unauthorized :
                        _logger.LogInformation($"Authentication failed for user {model.Email}. Status: '{authResult.Status}'.");
                        result = Unauthorized();
                        break;
                    case BusinessLayer.AccountStatus.Locked :
                        _logger.LogInformation($"Authentication failed for user {model.Email}. Status: '{authResult.Status}'.");
                        result = StatusCode(423); // Http locked
                        break;
                    case BusinessLayer.AccountStatus.Unverified :
                        _logger.LogInformation($"Authentication failed for user {model.Email}. Status: '{authResult.Status}'.");
                        result = StatusCode(417); // Expectation failed
                        break;
                    case BusinessLayer.AccountStatus.GoodStanding :
                        var token = generateToken(authResult.UserProfile);     
                        result = Ok($"{{ \"Token\": \"{token}\" }}");
                        break;
                    default:
                        result = BadRequest();
                        break;
                }
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, $"Unable to authenticate user '{model.Email}'.");
                result = StatusCode(500);
            }

            return result;
        }

        #endregion

        #region Helper Methods

        private string generateToken(BusinessLayer.User user)
        {
            var result = string.Empty;
            
            var claims = new[]
            {
                new Claim("name", $"{user.FirstName} {user.LastName}"),
                new Claim(JwtRegisteredClaimNames.Sub, user.UserId.ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            var issuer = _configuration["JwtSettings:Issuer"];
            var audience = _configuration["JwtSettings:Audience"];
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtSettings:PublicKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(issuer, audience, claims, expires: DateTime.Now.AddMinutes(30),  signingCredentials: creds);
            var tokenHandler = new JwtSecurityTokenHandler();
            result = tokenHandler.WriteToken(token);
            return result;
        }        

        #endregion
    }
}