using System;

namespace OnlineBanking.Business.Model
{
    public class BankAccount
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public BankAccountType Type { get; set; }
        public string ReferenceNumber { get; set; }
        public float Balance {get; set; }
    }
}