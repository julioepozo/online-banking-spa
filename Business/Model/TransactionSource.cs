using System;

namespace OnlineBanking.Business.Model
{
    public class TransactionSource
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
    }
}