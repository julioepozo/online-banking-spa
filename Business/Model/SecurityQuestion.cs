namespace OnlineBanking.Business.Model
{
    public class SecurityQuestion
    {
        public string Id { get; set; }
        public string Description { get; set; }
    }
}