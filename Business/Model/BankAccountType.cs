using System;

namespace OnlineBanking.Business.Model
{
    public class BankAccountType
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool IsDebit { get; set; }
    }
}