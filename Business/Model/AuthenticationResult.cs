using System;

namespace OnlineBanking.Business.Model
{
    public class AuthenticationResult
    {
        public User UserProfile { get; set; }
        public AccountStatus Status { get; set; }
    }
}