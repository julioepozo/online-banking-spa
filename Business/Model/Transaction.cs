using System;

namespace OnlineBanking.Business.Model
{
    public class Transaction
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public TransactionType Type { get; set; }
        public TransactionSource Source { get; set; }
        public DateTime Date { get; set; }
        public float Amount {get; set; }
    }
}