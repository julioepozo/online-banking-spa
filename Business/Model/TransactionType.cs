using System;

namespace OnlineBanking.Business.Model
{
    public class TransactionType
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public int Multiplier { get; set; }
    }
}