using System;
using System.Collections.Generic;

namespace OnlineBanking.Business.Model
{
    public class User
    {
        public Guid UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string VerificationCode { get; set; }
        public DateTime VerificationDate { get; set; }
        public string Password { get; set; }
        public string PasswordSalt {get; set; }
        public DateTime PasswordChangedOn { get; set; }
        public byte FailedLoginAttempts { get; set; }
        public DateTime LastLoginOn { get; set; }
        public bool IsLocked { get; set; }
        public bool IsEnabled { get; set; }
        public DateTime CreatedOn { get; set; }
        public Dictionary<string, string> SecurityQuestions { get; set; }
    }
}