namespace OnlineBanking.Business.Model
{
    public enum AccountStatus
    {
        NotFound = 0,
        Disabled = 1,
        Locked = 2,
        Unverified = 3,
        GoodStanding = 4,
        Unauthorized = 5
    }
}