using System;
using System.Collections.Generic;
using OnlineBanking.Common.Security;
using DataAccessLayer = OnlineBanking.DataAccess.Entities;
using BusinessLayer = OnlineBanking.Business.Model;
using OnlineBanking.DataAccess;

namespace OnlineBanking.Business
{
    public class BankAccountManager : IBankAccountManager
    {
        #region Member Variables

        private IBankAccountRepository _bankAccountRepository;

        #endregion
        
        #region Constructors

        public BankAccountManager(IBankAccountRepository repository)
        {
            _bankAccountRepository = repository;
        }

        #endregion

        #region Operations

        public BusinessLayer.BankAccount Get(Guid Id)
        {
            BusinessLayer.BankAccount result = null;
            var dbItem = _bankAccountRepository.Get(Id);

            if(dbItem != null)
            {
                result = new BusinessLayer.BankAccount()
                {
                    Id = dbItem.Id,
                    Description = dbItem.Description,
                    ReferenceNumber = dbItem.ReferenceNumber,
                    Balance = dbItem.Balance,
                    Type = new BusinessLayer.BankAccountType() 
                    {
                        Id = dbItem.Type.Id,
                        Description = dbItem.Type.Description,
                        IsDebit = dbItem.Type.IsDebit
                    }
                };
            }

            return result;
        }

        public List<BusinessLayer.BankAccount> GetAll(Guid userId)
        {
            var result = new List<BusinessLayer.BankAccount>();
            var list = _bankAccountRepository.GetAll(userId);

            if(list != null)
            {
                foreach(DataAccessLayer.BankAccount item in list)
                {
                    result.Add(new BusinessLayer.BankAccount() 
                    {
                        Id = item.Id,
                        Description = item.Description,
                        ReferenceNumber = item.ReferenceNumber,
                        Balance = item.Balance,
                        Type = new BusinessLayer.BankAccountType() 
                        {
                            Id = item.Type.Id,
                            Description = item.Type.Description,
                            IsDebit = item.Type.IsDebit
                        }
                    });
                }
            }

            return result;
        }

        #endregion
    }
}