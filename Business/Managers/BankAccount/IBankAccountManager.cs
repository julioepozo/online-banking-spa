using System;
using System.Collections.Generic;
using OnlineBanking.Business.Model;

namespace OnlineBanking.Business
{
    public interface IBankAccountManager
    {
        List<BankAccount> GetAll(Guid userId);
        BankAccount Get(Guid Id);
    }
}