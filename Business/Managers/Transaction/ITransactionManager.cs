using System;
using System.Collections.Generic;
using OnlineBanking.Business.Model;

namespace OnlineBanking.Business
{
    public interface ITransactionManager
    {
        List<Transaction> GetAll(Guid accountId);
    }
}