using System;
using System.Collections.Generic;
using OnlineBanking.Common.Security;
using DataAccessLayer = OnlineBanking.DataAccess.Entities;
using BusinessLayer = OnlineBanking.Business.Model;
using OnlineBanking.DataAccess;

namespace OnlineBanking.Business
{
    public class TransactionManager : ITransactionManager
    {
        #region Member Variables

        private ITransactionRepository _transactionRepository;

        #endregion
        
        #region Constructors

        public TransactionManager(ITransactionRepository repository)
        {
            _transactionRepository = repository;
        }

        #endregion

        #region Operations

        public List<BusinessLayer.Transaction> GetAll(Guid accountId)
        {
            var result = new List<BusinessLayer.Transaction>();
            var list = _transactionRepository.GetAll(accountId);

            if(list != null)
            {
                foreach(DataAccessLayer.Transaction item in list)
                {
                    result.Add(new BusinessLayer.Transaction() 
                    {
                        Id = item.Id,
                        Description = item.Description,
                        Date = item.Date,
                        Amount = item.Amount,
                        Type = new BusinessLayer.TransactionType() 
                        {
                            Id = item.Type.Id,
                            Description = item.Type.Description,
                            Multiplier = item.Type.Multiplier
                        },
                        Source = new BusinessLayer.TransactionSource() 
                        {
                            Id = item.Type.Id,
                            Description = item.Type.Description
                        }
                    });
                }
            }

            return result;
        }

        #endregion
    }
}