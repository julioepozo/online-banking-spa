using System;
using System.Collections.Generic;
using OnlineBanking.Business.Model;

namespace OnlineBanking.Business
{
    public interface IUserManager
    {
        bool Add(User user);
        List<User> GetAll();
        AuthenticationResult Authenticate(string email, string password);
        User Get(Guid userId);
        bool Delete(Guid userId);
        bool Update(Guid userId, User user);
    }
}