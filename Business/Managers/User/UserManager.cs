using System;
using System.Collections.Generic;
using OnlineBanking.Common.Security;
using DataAccessLayer = OnlineBanking.DataAccess.Entities;
using BusinessLayer = OnlineBanking.Business.Model;
using OnlineBanking.DataAccess;
using System.Security.Claims;

namespace OnlineBanking.Business
{
    public class UserManager : IUserManager
    {
        #region Member Variables

        private IUserRepository _userRepository;

        #endregion
        
        #region Constructors

        public UserManager(IUserRepository repository)
        {
            _userRepository = repository;
        }

        #endregion

        #region Operations

        public BusinessLayer.AuthenticationResult Authenticate(string email, string password)
        {
            var result = new BusinessLayer.AuthenticationResult();
            result.UserProfile = getByEmail(email);
            result.Status = getAccountStatus(result.UserProfile, password);
            return result;
        }
        
        public BusinessLayer.User Get(Guid userId)
        {
            BusinessLayer.User result = null;
            var dbItem = _userRepository.Get(userId);

            if(dbItem != null)
                result = mapToBlUser(dbItem);

            return result;
        }

        public List<BusinessLayer.User> GetAll()
        {
            var result = new List<BusinessLayer.User>();
            var list = _userRepository.GetAll();

            if(list != null)
            {
                foreach(DataAccessLayer.User item in list)
                {
                    result.Add(new BusinessLayer.User() 
                    {
                        UserId = item.UserId, 
                        FirstName = item.FirstName,
                        LastName = item.LastName, 
                        Email = item.Email
                    });
                }
            }

            return result;
        }

        public bool Add(BusinessLayer.User blItem)
        {
            var passwordSalt = Guid.NewGuid().ToString();

            var dbItem = new DataAccessLayer.User()
            {
                UserId = Guid.NewGuid(),
                FirstName = blItem.FirstName,
                LastName = blItem.LastName,
                Password = EncryptionHelper.GenerateSaltedHash(blItem.Password, passwordSalt, 0),
                PasswordSalt = passwordSalt,
                Email = blItem.Email,
                SecurityQuestions = blItem.SecurityQuestions
            };

            return _userRepository.Add(dbItem);
        }

        public bool Delete(Guid userId)
        {
            return _userRepository.Delete(userId);
        }

        public bool Update(Guid userId, BusinessLayer.User blItem)
        {
            var dbItem = new DataAccessLayer.User()
            {
                UserId = userId,
                FirstName = blItem.FirstName,
                LastName = blItem.LastName,
                Password = EncryptionHelper.GenerateSaltedHash(blItem.Password, blItem.PasswordSalt, 0),
                Email = blItem.Email
            };

            return _userRepository.Update(dbItem);
        }

        #endregion

        #region Helper Methods

        private BusinessLayer.User getByEmail(string email)
        {
            BusinessLayer.User result = null;
            var dbItem = _userRepository.GetByEmail(email);

            if(dbItem != null)
                result = mapToBlUser(dbItem);

            return result;
        }

        private BusinessLayer.AccountStatus  getAccountStatus(BusinessLayer.User user, string suppliedPassword)
        {
            if(user == null)
                return BusinessLayer.AccountStatus.NotFound;

            if(!user.IsEnabled)
                return BusinessLayer.AccountStatus.Disabled;

            if(user.IsLocked)
                return BusinessLayer.AccountStatus.Locked;

            if(string.IsNullOrEmpty(user.VerificationCode))
                return BusinessLayer.AccountStatus.Unverified;

            if(passwordMatches(user, suppliedPassword))
                return BusinessLayer.AccountStatus.GoodStanding;

            return BusinessLayer.AccountStatus.Unauthorized;
        }

        private bool passwordMatches(BusinessLayer.User user, string suppliedPassword)
        {
            var saltedPassword = EncryptionHelper.GenerateSaltedHash(suppliedPassword, user.PasswordSalt);
            return saltedPassword == user.Password;
        }

        private BusinessLayer.User mapToBlUser(DataAccessLayer.User dbItem)
        {
            var result = new  BusinessLayer.User()
            { 
                UserId = dbItem.UserId,
                FirstName = dbItem.FirstName,
                LastName = dbItem.LastName,
                Password = dbItem.Password,
                PasswordSalt = dbItem.PasswordSalt,
                PasswordChangedOn = dbItem.PasswordChangedOn,
                LastLoginOn = dbItem.LastLoginOn,
                FailedLoginAttempts = dbItem.FailedLoginAttempts,
                Email = dbItem.Email, 
                CreatedOn = dbItem.CreatedOn,
                IsLocked = dbItem.IsLocked,
                IsEnabled = dbItem.IsEnabled,
                VerificationCode = dbItem.VerificationCode,
                VerificationDate = dbItem.VerificationDate
            };
            
            return result;
        }

        #endregion
    }
}