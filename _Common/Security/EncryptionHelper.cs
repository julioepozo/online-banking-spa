using System;
using System.Text;
using System.Security.Cryptography;

namespace OnlineBanking.Common.Security
{
    public static class EncryptionHelper
    {
        public static string HMACHash(string data, string secret)
        {
            var result = string.Empty;
            var keyBytes = Encoding.UTF8.GetBytes(secret);
            var dataBytes = Encoding.UTF8.GetBytes(data);

            using (HMACSHA256 hmac = new HMACSHA256(keyBytes))
            {
                result = Convert.ToBase64String(hmac.ComputeHash(dataBytes));
            }

            return result;
        }

        public static string GenerateRandomNumber()
        {
            var result = string.Empty;
            var saltBytes = new byte[32];
            
            using(var random = new RNGCryptoServiceProvider())
            {
                random.GetBytes(saltBytes);
                result = Convert.ToBase64String(saltBytes);
            }

            return result;
        }

        public static string GenerateSaltedHash(string data, string salt, int delay = 0)
        {
            var result = string.Empty;
            var dataBytes = Encoding.UTF8.GetBytes(data + salt);
            
            using (var sha512 = new SHA512CryptoServiceProvider())
            {
                var hashBytes = sha512.ComputeHash(dataBytes);
                result = Convert.ToBase64String(hashBytes);
            }

            return result;
        }
    }
}
