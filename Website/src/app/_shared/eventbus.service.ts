import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';

@Injectable()
export class EventBus 
{
  private userAuthenticated = new Subject<string>();  
  userAuthenticated$ = this.userAuthenticated.asObservable();

  private onError = new Subject();  
  onError$ = this.onError.asObservable();

  private onBusy = new Subject();  
  onBusy$ = this.onBusy.asObservable();

  private onIdle = new Subject();  
  onIdle$ = this.onIdle.asObservable();

  notifyBusy()
  {
    this.onBusy.next();
  }

  notifyIdle()
  {
    this.onIdle.next();
  }
  
  notifyLogin(data: string) 
  {
    this.userAuthenticated.next(data);
  }

  notifyError()
  {
    this.onError.next();
  }
}