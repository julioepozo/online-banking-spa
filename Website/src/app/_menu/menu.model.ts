export class MenuOption 
{
    caption: string;
    route: string;
}