import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { EventBus } from '../_shared/eventbus.service';
import { MenuOption } from './menu.model';
import { MenuService } from './menu.service';

@Component
({
  selector: 'main-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})

export class MenuComponent
{
  userName = sessionStorage.getItem('user-name');
  menuOptions: MenuOption[];

  constructor(private menuService: MenuService, private router: Router, private eventBus: EventBus)
  {
    this.menuOptions = this.menuService.getList();    
    this.eventBus.userAuthenticated$.subscribe
    (
      data => 
      { 
          this.userName = data; 
      }
    );
  }

  toggleMenu(e)
  {
    if(!isMobile())
      return false;
    
    var mobileMenu = document.getElementById('main-menu-body');
    
    if(mobileMenu.style.display == '' || mobileMenu.style.display == "none")
      mobileMenu.style.display = "inline-block";
    else
      mobileMenu.style.display = "none";
  }

  logOut()
  {
    this.userName = '';
    sessionStorage.clear();
    this.toggleMenu(null);
    this.router.navigate(['/']);
  }
}

function isMobile()
{
  var mq = window.matchMedia('(max-width: 414px)');
  return mq.matches;
}
