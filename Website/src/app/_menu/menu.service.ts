import { Injectable } from '@angular/core';
import { MenuOption } from './menu.model';

@Injectable()
export class MenuService {
    
    constructor() { }

    getList() : MenuOption[]
    {
        var list = new Array<MenuOption>();
        list.push( { caption: 'Accounts', route: '/accounts' } );
        list.push( { caption: 'Transfers', route: '/transfers' } );
        list.push( { caption: 'Statements', route: '/statements' } );
        return list;
    }
}