import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { HttpHeaders } from '@angular/common/http/src/headers';

@Component
({
  selector: 'transfers',
  templateUrl: './transfers.component.html',
  styleUrls: ['./transfers.component.css']
})

export class TransfersComponent
{

  constructor(private http: HttpClient, private router: Router)
  {
    var options = { headers: { 'Authorization' : 'Bearer ' + sessionStorage.getItem('token') } };

    this.http.get
    (
      'http://localhost:5000/api/user', options )
      .subscribe(data => 
        { 
        },
        error =>
        {
          if(error.status ==  401)
          {
            router.navigate(['/login']);
            sessionStorage.setItem('next-route', '/transfers');
          }
        }
    );
  }

}
