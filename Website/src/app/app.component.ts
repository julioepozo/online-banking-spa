import { Component, Output } from '@angular/core';
import { EventBus } from './_shared/eventbus.service';
import { SecurityService } from './_security/security.service';

@Component
({
  providers: [EventBus],
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent
{
  isBusy = false;
  displayError: boolean;
  
  constructor(private eventBus: EventBus)
  {
    this.eventBus.onBusy$.subscribe(() => { this.isBusy = true; });
    this.eventBus.onIdle$.subscribe(() => { this.isBusy = false; });
    this.eventBus.onError$.subscribe(() => { this.displayError = true; });
  }    

  hideMenu(e)
  {
    if(isMobile())
    {
      var mobileMenu = document.getElementById('main-menu-body');
      mobileMenu.style.display = "none";
    }
  }
}

function isMobile()
{
  var mq = window.matchMedia('(max-width: 414px)');
  return mq.matches;
}
