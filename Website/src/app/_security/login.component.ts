import { Component, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { SecurityService } from './security.service';
import { EventBus } from '../_shared/eventbus.service';

@Component
({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent 
{
  title = 'Authentication';
  email: string;
  password: string;
  errorMessage: string;

  constructor(private security: SecurityService, private router: Router, private eventBus: EventBus) 
  {
    this.email = "jpozo@yahoo.com";
    this.password = "Password1";
  }
  
  login()
  {
    this.eventBus.notifyBusy();
    this.security.authenticate(this.email, this.password)
      .subscribe
      (
        data => { this.handleSuccess(data) },
        error => { this.handleError(error.status) }
      );
  }

  private handleSuccess(data: any)
  {
    var result = JSON.parse(data);
    var jwt = this.security.parseJwt(result.Token); 
    this.setSessionValues(result.Token, jwt);
    this.eventBus.notifyLogin(jwt.name);
    this.router.navigate([sessionStorage.getItem('next-route')]);
    sessionStorage.removeItem('next-route');
    this.eventBus.notifyIdle();
  }

  private handleError(statusCode: number)
  {
    console.log(statusCode)
    switch(statusCode)
    {
      case 400 :
      case 401 :
      case 403 :
        this.errorMessage = "Invalid user id and/or password.";
        break;
      case 417 :
        this.errorMessage = "Online access for this account is still in progress.";
        break;
      case 423 :
        this.errorMessage = "Please reset your password.";
        break;
      default :
        this.eventBus.notifyError();  
        break;
    }
    
    this.eventBus.notifyIdle();
  }

  private setSessionValues(rawToken: string, jwt: any)
  {
    sessionStorage.setItem("token", rawToken);
    sessionStorage.setItem("expiry", jwt.exp);
    sessionStorage.setItem("user-id", jwt.sub);
    sessionStorage.setItem("user-name", jwt.name);
  }
}