export class Registration
{
  FirstName: string;
  LastName: string;
  Email: string;
  EmailConfirmation: string;
  Password: string;
  PasswordConfirmation: string;
};