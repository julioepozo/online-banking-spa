import { Component, OnInit, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { SecurityService } from './security.service';
import { EventBus } from '../_shared/eventbus.service';
import { Registration } from './security.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'selector-name',
    templateUrl: 'registration.component.html',
    styleUrls: ['./registration.component.css']
})

export class RegistrationComponent implements OnInit {

    title = 'User Registration';
    errorMessage = '';
    model = new Registration();
    regForm: FormGroup;

    constructor(private security: SecurityService, private router: Router, private eventBus: EventBus) 
    { 
    }

    ngOnInit() 
    { 
      this.regForm = new FormGroup
      ({ 
        'firstName' : new FormControl( this.model.FirstName, [Validators.required] ),
        'lastName' : new FormControl( this.model.LastName, [Validators.required] ),
        'email' : new FormControl( this.model.Email, [Validators.required] ),
        'emailConfirmation' : new FormControl( this.model.EmailConfirmation, [Validators.required] ),
        'password' : new FormControl( this.model.Password, [Validators.required] ),
        'passwordConfirmation' : new FormControl( this.model.PasswordConfirmation, [Validators.required] ),
      });
    }

    get firstName() { return this.regForm.get('firstName'); }
    get lastName() { return this.regForm.get('lastName'); }
    get email() { return this.regForm.get('email'); }
    get emailConfirmation() { return this.regForm.get('emailConfirmation'); }
    get password() { return this.regForm.get('password'); }
    get passwordConfirmation() { return this.regForm.get('passwordConfirmation'); }

    register()
    {
        this.eventBus.notifyBusy();

        this.security.register(this.model)
          .subscribe(
            data =>
            {
              this.eventBus.notifyIdle();
              this.router.navigate(['/login']);
            },
            error =>
            {
              if(error.status >= 400  && error.status <= 499)
              {
                this.errorMessage = "The provided information is invalid.";
              }
              else
              {
                this.eventBus.notifyError();
              }
    
              this.eventBus.notifyIdle();
            }
          );
    }
}