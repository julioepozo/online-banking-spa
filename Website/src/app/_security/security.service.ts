import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Environment } from '../../environments/environment';
import { Registration } from './security.model';

@Injectable()
export class SecurityService 
{
    constructor(private http: HttpClient)
    {
    }

    authenticate(email: string, password: string) 
    {
        var data = { 'Email' : email, 'Password' : password };
        var uri = Environment.apiGatewayRoot + Environment.apiGatewayUris.authentication; 
        return this.http.post(uri, data, { responseType: 'text' });
    }

    register(model: Registration)
    {
        var uri = Environment.apiGatewayRoot + Environment.apiGatewayUris.registration; 
        return this.http.post(uri, model, { responseType: 'text' });
    }

    parseJwt (token) 
    {
      var base64Url = token.split('.')[1];
      var base64 = base64Url.replace('-', '+').replace('_', '/');
      return JSON.parse(window.atob(base64));
    };
}