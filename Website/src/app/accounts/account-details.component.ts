import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { EventBus } from '../_shared/eventbus.service';
import * as Model from './account.model';
import { AccountService } from './account.service';

@Component
({
    selector: 'account-details',
    templateUrl: 'account-details.component.html',
    styleUrls: ['account-details.component.css']
})

export class AccountDetailsComponent implements OnInit 
{
    BankAccount: Model.BankAccount;
    Transactions: Model.Transaction[];
    Message = '';

    constructor(private accountService: AccountService, private router: Router, 
      private eventBus: EventBus, private currentRoute: ActivatedRoute) { }
    
    ngOnInit()
    {
      this.eventBus.notifyBusy();
      var accountId = this.currentRoute.snapshot.params['id'];
      this.getBankAccount(accountId);
      this.getTransactions(accountId);
    }

    getBankAccount(accountId: string)
    {
      return this.accountService.get(accountId)
        .subscribe(data => 
          { 
            this.BankAccount = data as Model.BankAccount;
          },
          error =>
          {
            switch(error.status)
            {
              case 401 :
                this.router.navigate(['/login']);
                sessionStorage.setItem('next-route', '/accounts');
              break;
              case 404 :
                this.Message = 'This account does not exist';
              break;
              default:
                this.eventBus.notifyError();
            }
          }
      );
    }

    getTransactions(accountId: string)
    {
      this.accountService.getTransactions(accountId)
        .subscribe(data => 
          { 
            this.Transactions = data as Array<Model.Transaction>;
            this.eventBus.notifyIdle();
          },
          error =>
          {
            switch(error.status)
            {
              case 401 :
                this.router.navigate(['/login']);
                sessionStorage.setItem('next-route', '/accounts');
              break;
              case 404 :
                this.Message = 'This account has no transactions';
              break;
              default:
                this.eventBus.notifyError();
            }

            this.eventBus.notifyIdle();
          }
      );
  }
}