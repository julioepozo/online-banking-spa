import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BankAccount } from './account.model';
import { Environment } from '../../environments/environment';

@Injectable()
export class AccountService 
{
    constructor(private httpClient: HttpClient) { }

    getList(userId: string)
    {
        var options = { headers: { 'Authorization' : 'Bearer ' + sessionStorage.getItem('token') } };
        var uri = Environment.apiGatewayRoot + Environment.apiGatewayUris.bankAccount + '/list/' + userId ;         
        return this.httpClient.get(uri, options);
    }

    get(accountId: string)
    {
        var options = { headers: { 'Authorization' : 'Bearer ' + sessionStorage.getItem('token') } };
        var uri = Environment.apiGatewayRoot + Environment.apiGatewayUris.bankAccount + '/' + accountId;         
        return this.httpClient.get(uri, options);
    }

    getTransactions(accountId: string)
    {
        var options = { headers: { 'Authorization' : 'Bearer ' + sessionStorage.getItem('token') } };
        var uri = Environment.apiGatewayRoot + Environment.apiGatewayUris.transaction + "/" + accountId;         
        return this.httpClient.get(uri, options);
    }
}