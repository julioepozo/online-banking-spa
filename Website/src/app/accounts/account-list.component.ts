import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as Model from './account.model';
import { EventBus } from '../_shared/eventbus.service';
import { AccountService } from './account.service';

@Component
({
  selector: 'transfers',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.css']
})

export class AccountListComponent implements OnInit
{
  BankAccounts: Model.BankAccount[];

  constructor(private accounts: AccountService, private router: Router, private eventBus: EventBus)
  {
  }

  ngOnInit()
  {
    this.eventBus.notifyBusy();
    var userId = sessionStorage.getItem('user-id');

    this.accounts.getList(userId)
      .subscribe(data => 
        { 
          this.BankAccounts = data as Array<Model.BankAccount>;
        },
        error =>
        {
          this.handleError(error.status);
        }
    );

    this.eventBus.notifyIdle();
  }

  handleError(httpStatus: number)
  {
    switch(httpStatus)
    {
      case 400 :
      case 401 :
        this.router.navigate(['/login']);
        sessionStorage.setItem('next-route', '/accounts');
        break;
      case 404 :
        break;
      default:
        this.eventBus.notifyError();
    }
  }
}