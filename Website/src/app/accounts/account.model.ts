
export class BankAccountType
{
  id: number;
  description: string;
  isDebit: boolean;
};

export class BankAccount 
{
  id: string;
  referenceNumber: string;
  type: BankAccountType;
  description: string;
  balance: number;
}

export class TransactionType
{
  id: string;
  description: string;
  multiplier: number;
}

export class TransactionSource
{
  id: string;
  description: string;
};

export class Transaction
{
  id: string;
  description: string;
  date: Date;
  amount: number;
  type: TransactionType;
  source: TransactionSource;
};