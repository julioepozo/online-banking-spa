import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { MenuComponent } from './_menu/menu.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { RegistrationComponent } from './_security/registration.component';
import { LoginComponent } from './_security/login.component';
import { AccountListComponent } from './accounts/account-list.component';
import { AccountDetailsComponent } from './accounts/account-details.component';
import { TransfersComponent } from './transfers/transfers.component';
import { PaymentsComponent } from './payments/payments.component';

import { Router } from '@angular/router';
import { EventBus } from './_shared/eventbus.service';
import { SecurityService } from './_security/security.service';
import { MenuService } from './_menu/menu.service';
import { AccountService } from './accounts/account.service';

const appRoutes: Routes = 
[
  { path: '', component: AccountListComponent },
  { path: 'accounts', component: AccountListComponent },
  { path: 'account-details/:id', component: AccountDetailsComponent,  },
  { path: 'transfers', component: TransfersComponent },
  { path: 'payments', component: PaymentsComponent },
  { path: 'register', component: RegistrationComponent },
  { path: 'login', component: LoginComponent }
];

@NgModule
({
  declarations: 
  [ 
    AppComponent, 
    MenuComponent, 
    LoginComponent,
    RegistrationComponent,
    AccountListComponent,
    AccountDetailsComponent,
    TransfersComponent,
    PaymentsComponent
  ],
  imports: 
  [ 
    BrowserModule, 
    RouterModule.forRoot(appRoutes), 
    FormsModule, 
    ReactiveFormsModule,
    HttpClientModule
  ],
  exports: [],
  providers: 
  [ 
    SecurityService, 
    MenuService, 
    AccountService,
    EventBus, 
    HttpClientModule 
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
