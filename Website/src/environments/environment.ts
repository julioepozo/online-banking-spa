import { NAMESPACE_URIS } from "@angular/platform-browser/src/dom/dom_renderer";

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const Environment = 
{
  production: false,

  apiGatewayRoot: 'http://localhost:5000/api/',

  apiGatewayUris: 
  {
    registration: 'user',
    authentication: 'user/authenticate',
    bankAccount: 'bankaccount',
    transaction: 'transaction'
  }
};
